/*********************************************************************
  Drive Monochrome OLEDs based on SSD1306 drivers via Serial.
  This example is for a 128x32 size display using I2C to communicate
  3 pins are required to interface (2 I2C and one reset)

  chain Arduinos, each driving one Oled.

  Works like ws2810 drivers. only one single wire (from Software serial out to rx). (plus gnd and vcc)
  data format
  startbit (254), formatbit (below), message (max20), stopbyte (253)

  formatbyte:
  - %2 - big or small.

  
  
*********************************************************************/

#include <SPI.h>
#include <Wire.h>

#include <SoftwareSerial.h>

SoftwareSerial mySerial (2, 3); // RX, TX

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// fro printbig
#include <Fonts/FreeSansBold18pt7b.h>

//for printsmall
#include <Fonts/FreeSans12pt7b.h>
//#include <Fonts/FreeMonoBold12pt7b.h>

const byte START_BYTE = 254;
const byte STOP_BYTE = 253;

#define OLED_RESET -1

Adafruit_SSD1306 display(OLED_RESET);

int LED_PIN = 13;

String input_string;
bool print_big = false;
bool for_me = false;
byte address;
byte read_cycle = 0;
char data[20];
char data_length;

void display_data()
{
  char output_chars[data_length];

  for (int i = 0; i < data_length; i++) // process data to string
  {
    output_chars[i] = data[i];
  }
  String output_string = String(output_chars);

  if (print_big)
  {
    printFormattedBig(output_string);
  }
  else
  {
    printFormattedSmall(output_string);
  }
}

void setup()   {
  pinMode(LED_PIN, OUTPUT);
  
  Serial.begin(4800);

  mySerial.begin(4800);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  printFormattedSmall("  -no data.-");
}

void loop() {
  SerialReceive();
}
