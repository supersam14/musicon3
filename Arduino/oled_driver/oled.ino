void printFormattedBig(String data)
{
  display.clearDisplay();
  display.drawRect(0, 0, display.width(), display.height(), WHITE);
  //display.setTextColor(BLACK, WHITE); // 'inverted' text
  display.setFont(&FreeSansBold18pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(3, 28);
  for(int i = 0; i < data.length(); i++)
  {
    display.print(data[i]);
  }
  
  display.display(); // flip
}

void printFormattedSmall(String data)
{
  display.clearDisplay();
  
  display.drawLine(0, 3, 128, 3, WHITE);
  display.drawLine(0, 29, 128, 29, WHITE);
  //display.drawRect(0, 0, display.width(), display.height(), WHITE);
  //display.setTextColor(BLACK, WHITE); // 'inverted' text
  display.setFont(&FreeSans12pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(3, 25);
  for(int i = 0; i < data.length(); i++)
  {
    display.print(data[i]);
  }
  
  display.display(); // flip  
}

void measureWriteTime(String data)
{
  long start_time = millis();
    
  printFormattedBig(data);
  
  display.display(); // flip

  long end_time = millis();

  long dur = end_time - start_time;

  //display.setTextColor(BLACK, WHITE); // 'inverted' text
  display.clearDisplay();
  display.setFont();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(3, 27);
  display.print(dur);
  display.display();
  
}

void print_fourchars(char data[4])
{
  display.clearDisplay();
  display.setCursor(2, 2);
  display.setTextSize(4);
  display.setTextColor(WHITE);
  
  for(int i = 0; i < 4; i++)
  {
    display.print(data[i]);
  }
  
  display.display(); // flip
  digitalWrite(LED_PIN, LOW);
}
