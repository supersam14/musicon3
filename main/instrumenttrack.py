# Sequencer main

# one Instrument Track per different sequencer instance. this Instrument Track contains notetracks containing notes.
# notetracks manually in setup of the different sequencer instances

import time

class InstrumentTrack:
    def __init__(self, track_name):

        self.name = track_name

        self.step_amount = 128

        self.last_step = self.step_amount

        self.mute_flag = False

        self.current_step = 0

        self.note_tracks = []

    def addNoteTrackSinglePitch(self, instrument_name, pitch):
        track = NoteTrackSinglePitch(instrument_name, self.step_amount, pitch)
        self.note_tracks.append(track)

class NoteTrackSinglePitch:
    def __init__(self, instrument_name, step_amount, pitch):

        self.name = instrument_name # showing on screen or so...

        self.pitch = pitch # one single pitch

        self.step_amount = step_amount
        self.current_step = 0
        self.last_step = self.step_amount - 1

        self.mute_flag = False

        self.notes = []

        for i in range(self.step_amount):
            self.notes.append(Note(self.pitch))

    def tick(self): # this function is accessed by the master timer. updates the notetracks current step and returns the note
                    # to be played
        res = None

        # go through notes to find the correct note to be played
        if self.notes[self.current_step].velocity > 0 and self.notes[self.current_step].previous_velocity == 0:
            res = self.notes[self.current_step]

        elif self.notes[self.current_step].velocity == 0 and self.notes[self.current_step].previous_velocity > 0:
            res = self.notes[self.current_step]

        # count the step
        self.current_step += 1
        if self.current_step > self.last_step:
            self.current_step = 0

        return res

# set parameters
    def set_current_step(self, step):
        if step > self.last_step:
            print('Error in NoteTrackSinglePitch.setCurrentStep: invalid input')
            print('input value:'),
            print(step),
            print(' currentStep:'),
            print(self.current_step)

            while True: # wait for infinity
                time.sleep(1)

        self.current_step = step

    def set_last_step(self, step):
        if step > self.step_amount or step < 0:
            print('Error in NoteTrackSinglePitch.setLast Step: invalid input')
            print('input value:'),
            print(step),
            print(' step amount:'),
            print(self.step_amount)

            while True:  # wait for infinity
                time.sleep(1)

        self.last_step = step

    def mute(self):
        self.mute_flag = True

    def unmute(self):
        self.mute_flag = False

# set notes
    def set_note_vel(self, index, vel):
        if vel < 0 or vel > 127:
            print('Error: Invalid Velocity value in set_note: '),
            print(vel)
            while True:
             time.sleep(1)

        self.notes[index].setVelocity(vel)


# class NotetrackMonophone:
#     def __init__(self, length, lowest_pitch, hightest_pitch):
#         self.lowest_pitch = lowest_pitch
#         self.highest_pitch = hightest_pitch
#         self.length = length
#
#
#         self.tone_amount = self.highest_pitch - self.lowest_pitch
#         self.tones = []
#
#         for tone in range(len(self.tone_amount)):
#             self.tones.append(Note(lowest_pitch + tone))

class Note:
    def __init__(self, pitch):
        self.pitch = pitch
        self.velocity = 0 # contains the velocity values
        self.previous_velocity = 0

        # self.glide = False
        # self.starting_note = False    to control the note-length

    def setVelocity(self, vel):
        if vel > 127 or vel < 0:
            print('Error in notes.setVelocity: invalid velocity')
            print('Input: '),
            print(vel)
            while True:
                time.sleep(1)

        self.previous_velocity = self.velocity
        self.velocity = vel
