############################ ANIMATION FRAMEWORK #######################################################################

import time


class Animation:
    def __init__(self):
        self.flag_down = False
        self.flag_up = False
        self.value = 0.0
        self.start_value = 0.0
        self.target_value = 0.0
        self.time_to_target = 0.0
        self.start_time = time.time()
        self.target_time = time.time()

    def animate(self):
        if self.flag_up:
            # calculations hjere
            time_since_start = time.time() - self.start_time
            progress = time_since_start / self.time_to_target
            value_diff = self.target_value - self.start_value

            self.value = self.start_value + value_diff * progress

            if self.value >= self.target_value:
                self.value = self.target_value
                self.flag_up = False

        elif self.flag_down:
            # calculations here
            time_since_start = time.time() - self.start_time
            progress = time_since_start / self.time_to_target
            value_diff = self.start_value - self.target_value

            self.value = self.start_value - value_diff * progress

            if self.value <= self.target_value:
                self.value = self.target_value
                self.flag_down = False

        if self.value > 1:
            self.value = 1.0
        if self.value < 0:
            self.value = 0.0

        return self.value


    def set_anim(self, target, time_to_target):

        if self.value < target:
            self.flag_down = True
        elif self.value > target:
            self.flag_down = True
        else:
            return False

        self.flag_up = True
        self.target_value = target
        self.start_value = self.value
        self.time_to_target = time_to_target
        self.start_time = time.time()
        self.target_time = self.start_time + self.time_to_target
