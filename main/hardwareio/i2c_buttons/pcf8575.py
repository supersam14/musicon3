import smbus as smbus
import time
import config
from hardwareio.i2c_buttons.matrix_config import *

class PCF8575:
    def __init__(self, address): # rewrite i2c part
        self.address = address

        self.last_update = time.time()

        self.keys = []  # key class holds state info calculates debouncing
        for i in range(16):
            self.keys.append(Key())

        self.reading = []
        for i in range(16):
            self.reading.append(False)

        self.i2c = smbus.SMBus(1) # open I2C Bus

        data = 0xffff   # set all pins to input
        # print('PCF8575 at address 0x{0:2x} WRITE 0x{1:4x}'.format(i2c_addr, data))
        self.i2c.write_byte_data(self.address, data & 0xff, (data & 0xff) >> 8)
        # self.i2c.write_byte_data(self.address, data, data)


    def update(self):
        temp = self.i2c.read_word_data(self.address, 0)

        for i in range(16):
            if (temp % 2 == 1):
                self.reading[i] = True
            else:
                self.reading[i] = False
            temp = temp / 2

        for i in range(16):
            self.keys[i].update(self.reading[i])


    def read(self, index):
        # return self.reading[index]
        return self.keys[index].read()


class Key:
    def __init__(self):
        self.last_state = False
        self.state = False

        self.pressed_since = time.time()
        self.last_update = time.time()
        self.last_read = time.time()

        self.low_since = time.time()
        self.high_since = time.time()

        self.debounced = False

    def update(self, value):

        self.last_state = self.state
        self.state = not value

        if self.last_state and not self.state:
            self.low_since = time.time()
        if not self.last_state and self.state:
            self.high_since = time.time()


        if self.low_since + DEBOUNCE_TIME < time.time():
            self.debounced = False

        if self.high_since + DEBOUNCE_TIME < time.time():
            self.debounced = True

    def read(self):
        return self.debounced