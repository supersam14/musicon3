from button import Button
from hardwareio.i2c_buttons.matrix_config import *

class MatrixAPI:
    def __init__(self, i2c_address_list):
        self.ROW = len(i2c_address_list)
        self.COL = 16

        self.buttons = Button(i2c_address_list)

    def read(self, index):
        matrix_index = BUTTON_PIN_LIST[index]
        return self.buttons.read(matrix_index)

    def debug_read_buttons(self):

        for j in range(4):
            for i in range(self.COL):
                if self.COL+i < 10:
                    print(''),
                print(j*16+i),
                print(':'),
                print(self.read(j*16+i)),
                print("-"),
            print()
        print('-------------------')


    def update_buttons(self):
        self.buttons.update()

