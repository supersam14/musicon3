
# due to my wildass soldering job i had to provide decoding list to bring them in order:
BUTTON_PIN_LIST = [17, 18, 28, 27, 16, 23, 26, 22,
                   12, 14,  3,  0,  7,  4, 10,  6,

                   30, 29, 19, 20, 31, 24, 21, 25,
                   13,  2,  1, 15,  8,  5, 11,  9,

                   33, 34, 35, 36, 32, 39, 37, 38,
                   49, 50, 51, 48, 55, 52, 53, 54,

                   46, 45, 44, 43, 47, 40, 42, 41,
                   60, 61, 62, 63, 56, 59, 58, 57]


SERIAL_BAUDRATE = 115200
SERIAL_OUTPUT_GATEWAY = "/dev/ttyUSB0"
SERIAL_INPUT_GATEWAY = '/dev/ttyS0'
