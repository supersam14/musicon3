import time
import config

class TickLauncher:
    def __init__(self, main, midi):


        self.main = main
        self.midi = midi
        self.tick_length = 600000 * (1 / BPM)

        self.last_tick_time = time.time()
        self.next_tick_time = time.time() + self.tick_length
        pass

    def update(self):
        time_to_tick = self.next_tick_time - time.time()
        if time_to_tick < config.CPU_WAIT_FOR_TICK_TIME:
            self.waitForTick()

    def launchTick(self):
        # resetting timers
        self.last_tick_time = time.time()
        self.next_tick_time = time.time() + self.tick_length



        #   send notes to midiout.


    def waitForTick(self):

        # access all tonetracks to get notes
        # for if if if
        # check mutehandler here
            # midi.addjob(self, channel, note, velocity, on_off_flag):

        for tt in self.main.timeKeeper.tone_tracks:
            for nt in self.main.timeKeeper.tone_tracks.note_tracks:


                # if self.main.Timekeeper.tone_tracks.note_tracks.:
#                    if the right tick

                    if note.velocity > 0:

                        midi_channel = self.main.midi_channel
                        pitch = note.pitch
                        velocity = note.velocity

                        self.midi.addjob(midi_channel, pitch, velocity, True)

                    # else: noteoff

        # done writing list

        while time.time() < self.next_tick_time: # wait for the exact timing
            time.sleep(0.001)

            # maybe update animations here

        self.launchTick()

        pass